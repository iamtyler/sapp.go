package bus

import (
	"io"
)

type Message interface {
	ID() uint64
	Protocol() *string
	Name() *string
	Recipient() *string
}

type OutgoingMessage interface {
	Message
	io.Writer
}

type IncomingMessage interface {
	Message
	io.Reader
}
