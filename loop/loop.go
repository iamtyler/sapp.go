package loop

import (
	"sync"
	"time"
)

var (
	mutex sync.Mutex

	channel chan struct{}
	queue   *workerQueue
	doStop  bool
)

type Worker interface {
	Work()
}

type WorkerFunc func()

type FuncWorker struct {
	worker WorkerFunc
}

func NewWorker(worker WorkerFunc) *FuncWorker {
	return &FuncWorker{
		worker: worker,
	}
}

func (fw *FuncWorker) Work() {
	fw.worker()
}

func signal() {
	select {
	case channel <- struct{}{}:
	}
}

func Enqueue(w Worker) {
	mutex.Lock()
	queue.Push(w)
	mutex.Unlock()

	signal()
}

func Schedule(w Worker, delay time.Duration) {
	// TODO: figure out how to schedule a worker -- maybe with time.Timer?
}

func ScheduleAt(w Worker, when time.Time) {
	// TODO: figure out how to schedule a worker -- maybe with time.Timer?
}

func Run() {
	mutex.Lock()
	channel = make(chan struct{})
	queue = newWorkerQueue()
	mutex.Unlock()

	for _ = range channel {
		mutex.Lock()
		var w Worker
		for ; w != nil; w = queue.Pop() {
			mutex.Unlock()
			w.Work()
			mutex.Lock()
		}
		if doStop {
			close(channel)
		}
		mutex.Unlock()
	}
}

func Stop() {
	mutex.Lock()
	doStop = true
	mutex.Unlock()

	signal()
}
