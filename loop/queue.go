package loop

type workerQueue struct {
	workers []Worker
	head    int
	tail    int
	count   int
}

func newWorkerQueue() *workerQueue {
	return &workerQueue{
		workers: make([]Worker, 0),
	}
}

func (q *workerQueue) Push(worker Worker) {
	if q.head == q.tail && q.count > 0 {
		workers := make([]Worker, len(q.workers)*2)
		copy(workers, q.workers[q.head:])
		copy(workers[len(q.workers)-q.head:], q.workers[:q.head])
		q.head = 0
		q.tail = len(q.workers)
		q.workers = workers
	}
	q.workers[q.tail] = worker
	q.tail = (q.tail + 1) % len(q.workers)
	q.count++
}

func (q *workerQueue) Pop() Worker {
	if q.count == 0 {
		return nil
	}
	worker := q.workers[q.head]
	q.head = (q.head + 1) % len(q.workers)
	q.count--
	return worker
}
