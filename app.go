package sapp

import (
	"errors"
	"sync"
	"time"

	"gitlab.com/iamtyler/sapp/app"
	"gitlab.com/iamtyler/sapp/bus"
	"gitlab.com/iamtyler/sapp/cycle"
	"gitlab.com/iamtyler/sapp/loop"
)

type Stage int

const (
	New Stage = iota
	Starting
	Running
	Stopping
	Stopped
)

var (
	ErrStageNotNew = errors.New("the application can only start once")
)

var (
	mutex sync.Mutex

	stage   Stage = New
	doStop  bool
	started time.Time
)

func OpenNetwork() {
	// TODO: bring up network endpoints
}

func Run(a app.Application) error {
	// Initialize base subsystems
	// TODO: logging subsystem

	{
		mutex.Lock()
		defer mutex.Unlock()

		if stage != New {
			return ErrStageNotNew
		}
		stage = Starting
	}

	// Initialize subsystems
	// TODO: configuration subsystem
	// TODO: networking subsystem
	// TODO: http subsystem
	// TODO: websocket subsystem
	bus.Initialize()

	cycle.Register(a)
	loop.Enqueue(cycle.NewStartWorker(loop.NewWorker(OpenNetwork)))

	loop.Run()

	return nil
}

func Stop() {
	mutex.Lock()
	defer mutex.Unlock()

	if doStop {
		return
	}
	doStop = true

	// TODO: run teardown worker (close network, run cycle.StopWorker, stop subsystems)
}
