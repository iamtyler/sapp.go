package app

import (
	"gitlab.com/iamtyler/sapp/cycle"
)

type Application interface {
	cycle.StarterStopper

	Name() string
}
