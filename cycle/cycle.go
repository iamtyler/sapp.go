package cycle

import (
	"sync"
	"time"

	"gitlab.com/iamtyler/sapp/loop"
)

type Starter interface {
	Start()
	HasStarted() bool
}

type Stopper interface {
	Stop()
	HasStopped() bool
}

type StarterStopper interface {
	Starter
	Stopper
}

type entry struct {
	starter    Starter
	isStarting bool
	hasStarted bool

	stopper    Stopper
	isStopping bool
	hasStopped bool
}

func (e *entry) Start() bool {
	if e.hasStarted {
		return true
	}

	if !e.isStarting {
		e.isStarting = true
		e.starter.Start()
	}

	e.hasStarted = e.starter.HasStarted()
	return e.hasStarted
}

func (e *entry) Stop() bool {
	if e.hasStopped {
		return true
	}

	if !e.isStopping {
		e.isStopping = true
		e.stopper.Stop()
	}

	e.hasStopped = e.stopper.HasStopped()
	return e.hasStopped
}

var (
	mutex sync.Mutex

	entries = make([]*entry, 0)
)

const PollDelay = 5 * time.Second

type StartWorker struct {
	callback loop.Worker
}

func NewStartWorker(callback loop.Worker) *StartWorker {
	return &StartWorker{
		callback: callback,
	}
}

func (w *StartWorker) Work() {
	mutex.Lock()
	defer mutex.Unlock()

	ok := false
	for _, c := range entries {
		mutex.Unlock()
		if !c.Start() {
			ok = false
		}
		mutex.Lock()
	}

	if ok {
		if w.callback != nil {
			loop.Enqueue(w.callback)
		}
		return
	}

	loop.Schedule(w, PollDelay)
}

type StopWorker struct {
	callback loop.Worker
}

func (w *StopWorker) Work() {
	mutex.Lock()
	defer mutex.Unlock()

	ok := true
	for i := len(entries) - 1; i >= 0; i-- {
		c := entries[i]

		mutex.Unlock()
		if !c.Stop() {
			ok = false
		}
		mutex.Lock()
	}

	if ok {
		if w.callback != nil {
			loop.Enqueue(w.callback)
		}
		return
	}

	loop.Schedule(w, PollDelay)
}

func Register(ss StarterStopper) {
	mutex.Lock()
	defer mutex.Unlock()

	entries = append(entries, &entry{
		starter: ss,
		stopper: ss,
	})
}

func RegisterStarter(s Starter) {
	mutex.Lock()
	defer mutex.Unlock()

	entries = append(entries, &entry{
		starter:    s,
		hasStopped: true,
	})
}

func RegisterStopper(s Stopper) {
	mutex.Lock()
	defer mutex.Unlock()

	entries = append(entries, &entry{
		hasStarted: true,
		stopper:    s,
	})
}
